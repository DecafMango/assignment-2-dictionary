COMPILER = nasm
COMPILER_ATTRIBUTES = -f elf64 -o
LINKER = ld
LINKER_ATTRIBUTES = -o

.PHONY: all clean test

all: program

clean:
	rm *.o program

test:
	python3 test.py

%.o: %.asm
	$(COMPILER) $(COMPILER_ATTRIBUTES) $@ $<

program: main.o dict.o lib.o
	$(LINKER) $(LINKER_ATTRIBUTES) $@ $^
