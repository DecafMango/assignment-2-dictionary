%include "lib.inc"

section .text
global find_word

; find_word принимает на вход нуль-терминированную строку (rdi) и указатель на начало словаря (rsi)
find_word:
	push r12
	push r13
	mov r12, rdi ; Сделаем копии наших входных значений (чтобы в цикле не делать лишних обращений к памяти через push/pop)
	mov r13, rsi
	.loop:
		test r13, r13 ; Проверка на пустой узел (достижение конца словаря)
		jz .not_found

		mov rdi, r12
		lea rsi, [r13 + 8]
		call string_equals
		test rax, rax
		jnz .found
		mov r13, [r13]
		jmp .loop
	.found:
		mov rax, r13
		pop r13
		pop r12
		ret
	.not_found:
		xor rax, rax
		pop r13
		pop r12
		ret	
